const serviceCate = process.env.VUE_APP_SERVICE_AUTHS || ""
export const LOGIN_API = {
    url: serviceCate + '/user/login',
  };

  export const FORGOT_PASS = {
    url: serviceCate + '/user/retrieval/',
  };