const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || ""
export const GET_inspectionobject_API = {
    url: serviceBack + '/inspectionobject/',
  };
  export const ADD_inspectionobject_API = {
    url: serviceBack +'/inspectionobject/',
  };

  export const EDIT_inspectionobject_API = {
    url:serviceBack + '/inspectionobject/',
  };

  export const DEL_inspectionobject_API = {
    url:serviceBack + '/inspectionobject/',
  };
  export const GET_inspectionobject_API_By_IDPD = {
    url:serviceBack + '/inspectionobject/plandraft/',
  };
  export const GET_ALL_OBJECT = {
    url: serviceBack + '/inspectionobject/object/full',
  };
  export const ADD_MULTI_OBJECT = {
    url: serviceBack + '/inspectionobject/array',
  };
  export const PLEX_OBJECT = {
    url: serviceBack + '/plexplanation/',
  };