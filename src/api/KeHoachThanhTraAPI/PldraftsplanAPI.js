const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || ""
export const GET_Pldraftsplan_API = {
    url:serviceBack + '/pldraftsplan/',
  };
  export const ADD_Pldraftsplan_API = {
    url:serviceBack + '/pldraftsplan/',
  };
  export const ADDS_inspectionobject_API = {
    url:serviceBack + '/inspectionobject/array/',
  };
  export const EDIT_Pldraftsplan_API = {
    url:serviceBack + '/pldraftsplan/',
  };

  export const DEL_Pldraftsplan_API = {
    url:serviceBack + '/pldraftsplan/',
  };

  export const RETURN_Pldraftsplan_API = {
    url:serviceBack + '/pldraftsplan/changestatus/',
  };

  export const GUI_THAM_DINH = {
    url:serviceBack + '/pldraftsplan/sendappraisal/',
  };

