const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";

export const GET_HIS_REPORTING_PERIOD_API = {
  url: serviceCate + "/his_reporting_period/",
};

export const ADD_HIS_REPORTING_PERIOD_API = {
  url: serviceCate + "/his_reporting_period/",
};
