const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";
const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || "";

export const GET_DC_REPORTING_PERIOD_API = {
  url: serviceCate + "/dc_reporting_periods/",
};

export const ADD_DC_REPORTING_PERIOD_API = {
  url: serviceCate + "/dc_reporting_periods/",
};

export const GET_DC_REPORT_API = {
  url: serviceCate + "/dc_reports/",
};

export const GET_AGENCY_API = {
  url: serviceBack + "/govagencies/",
};

export const EDIT_DC_REPORTING_PERIOD_API = {
  url: serviceCate + "/dc_reporting_periods/",
};

export const BLOCK_DC_REPORTING_PERIOD_API = {
  url: serviceCate + "/dc_reporting_periods/block/",
};

export const DEL_DC_REPORTING_PERI_API = {
  url: serviceCate + "/dc_reporting_periods/",
};
