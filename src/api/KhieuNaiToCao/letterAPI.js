const serviceBack = process.env.VUE_APP_SERVICE_CATEGORY || "";
export const GET_LETTER_API = {
  url: serviceBack + "/letter/",
};
export const GET_APPROVE_LETTER_API = {
  url: serviceBack + "/letter/getapproveletters",
};
export const ADD_LETTER_API = {
  url: serviceBack + "/letter/",
};

export const EDIT_LETTER_API = {
  url: serviceBack + "/letter/",
};

export const DEL_LETTER_API = {
  url: serviceBack + "/letter/",
};
export const GET_LETTER_BY_ID_API = {
  url: serviceBack + "/letter/",
  method: "GET",
};

export const EDIT_CLOSE_WITHDRAW_LETTER_API = {
  url: serviceBack + "/letter/close-withdraw/",
};

export const EDIT_SEND_APPROVE_LETTER_API = {
  url: serviceBack + "/letter/send-approve/",
};

export const DELETE_FILE_BY_ID_API = {
  url: serviceBack + "/letter/delete-file-by-id/",
};

export const GET_CHECK_SOLUTION_API = {
  url: serviceBack + "/letter/check-solution/",
  method: "GET",
};
