const serviceHistory = process.env.VUE_APP_SERVICE_HISTORY || ""
export const GET_HISTORY_API = {
    url: serviceHistory + '/hisDraftPlan',
};
export const ADD_HISTORY_API = {
    url: serviceHistory + '/hisDraftPlan',
};
export const DEL_HISTORY_API = {
    url: serviceHistory + '/hisDraftPlan/',
};
