const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || ""
export const GET_COMPLAINTS_DENUNCIATION_FIELD_API = {
    url: serviceCate + '/complaintsdenunciationfield/',
  };

export const GET_COMPLAINTS_DENUNCIATION_FIELD_BY_ID_API = {
    url:  serviceCate +"/complaintsdenunciationfield/",
    method: "GET",
  };

  export const ADD_COMPLAINTS_DENUNCIATION_FIELD_API = {
    url:  serviceCate + '/complaintsdenunciationfield/',
  };

  export const EDIT_COMPLAINTS_DENUNCIATION_FIELD_API = {
    url:  serviceCate + '/complaintsdenunciationfield/',
  };

  export const DEL_COMPLAINTS_DENUNCIATION_FIELD_API = {
    url: serviceCate +  '/complaintsdenunciationfield/',
  };
