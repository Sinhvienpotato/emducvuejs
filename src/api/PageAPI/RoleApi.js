const serviceAuth = process.env.VUE_APP_SERVICE_AUTHS || ""
export const GET_ROLE_API = {
  url: serviceAuth + '/role',
};

export const ADD_ROLE_API = {
  url: serviceAuth + '/role',
};

export const EDIT_ROLE_API = {
  url: serviceAuth + '/role/',
};

export const DEL_ROLE_API = {
  url: serviceAuth + '/role/',
};


