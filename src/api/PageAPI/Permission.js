const serviceAuth = process.env.VUE_APP_SERVICE_AUTHS || ""
export const GET_PERMISSION_API = {
    url:serviceAuth + '/permission/',
  };

  export const ADD_PERMISSION_API = {
    url:serviceAuth + '/permission',
  };

  export const EDIT_PERMISSION_API = {
    url:serviceAuth + '/permission/',
  };

  export const DEL_PERMISSION_API = {
    url:serviceAuth + '/permission/',
  };

  