const serviceAuth = process.env.VUE_APP_SERVICE_AUTHS || ""
export const GET_GROUP_API = {
  url: serviceAuth + '/group/',
};

export const ADD_GROUP_API = {
  url: serviceAuth + '/group',
};

export const EDIT_GROUP_API = {
  url: serviceAuth + '/group/',
};

export const DEL_GROUP_API = {
  url: serviceAuth + '/group/',
};

