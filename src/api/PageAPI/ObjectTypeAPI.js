const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || ""
export const GET_OBJECT_TYPE_API = {
  url: serviceCate + '/objecttype/',
};
export const ADD_OBJECT_TYPE_API = {
  url: serviceCate + '/objecttype/',
};

export const EDIT_OBJECT_TYPE_API = {
  url: serviceCate + '/objecttype/',
};

export const DEL_OBJECT_TYPE_API = {
  url: serviceCate + '/objecttype/',
};

export const GET_All_SYMBOL_API = {
  url: serviceCate + '/objecttype/getallsymbol',
};
export const GET_LIST_BY_SYMBOL_API = {
  url: serviceCate + '/objecttype/getlistbysymbols',
};
