const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || ""
export const GET_FIELD_API = {
    url:serviceCate +  '/field/',
  };
  export const ADD_FIELD_API = {
    url:serviceCate +  '/field/',
  };

  export const EDIT_FIELD_API = {
    url: serviceCate + '/field/',
  };

  export const DEL_FIELD_API = {
    url:serviceCate +  '/field/',
  };
  export const GET_ALL_FIELD_API = {
    url:serviceCate +  '/field/getAll',
  };
