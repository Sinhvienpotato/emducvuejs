const serviceAuth = process.env.VUE_APP_SERVICE_AUTHS || "";
export const GET_USER_API = {
  url: serviceAuth + "/user/",
};

export const GET_USER_BY_GOV_API = {
  url: serviceAuth + "/user/govagencies/",
};

export const GET_USER_BY_DEP_API = {
  url: serviceAuth + "/user/department/",
};

export const ADD_USER_API = {
  url: serviceAuth + "/user/",
};

export const EDIT_USER_API = {
  url: serviceAuth + "/user/",
};

export const DEL_USER_API = {
  url: serviceAuth + "/user/",
};

export const CHANGE_PASS_USER = {
  url: serviceAuth + "/user/changepass/",
};

export const LOG_OUT = {
  url: serviceAuth + "/user/logout/",
};

export const CHANGE_PASS = {
  url: serviceAuth + "/user/changePassword/",
};

export const USER_INFO = {
  url: serviceAuth + "/user/check-exist/",
};
