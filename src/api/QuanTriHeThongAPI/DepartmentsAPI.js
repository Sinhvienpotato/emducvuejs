const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || ""
export const GET_Departments_API = {
    url:serviceBack + '/department/',
  };
  export const ADD_Departments_API = {
    url: serviceBack +'/department/',
  };

  export const EDIT_Departments_API = {
    url: serviceBack +'/department/',
  };

  export const DEL_Departments_API = {
    url: serviceBack +'/department/',
  };

  //tìm phòng ban theo id của đơn vị 
  export const GET_Departments_BY_ID_GOV = {
    url:serviceBack + '/department/govagencies/',
  };