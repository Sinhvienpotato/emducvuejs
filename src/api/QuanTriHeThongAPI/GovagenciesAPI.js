const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || "";
export const GET_govagencies_API = {
  url: serviceBack + "/govagencies/",
};
export const ADD_govagencies_API = {
  url: serviceBack + "/govagencies/",
};

export const EDIT_govagencies_API = {
  url: serviceBack + "/govagencies/",
};

export const DEL_govagencies_API = {
  url: serviceBack + "/govagencies/",
};

export const GET_govagencies_BY_level = {
  url: serviceBack + "/govagencies/level",
};
