const serviceBack = process.env.VUE_APP_SERVICE_CATEGORY || ""
const serviceBackend = process.env.VUE_APP_SERVICE_BACKEND || ""
export const GET_REPORT_02_XLD_API = {
  url:serviceBack + "/report02xld/",
};
export const ADD_REPORT_02_XLD_API  = {
  url:serviceBack +  "/report02xld/",
};

export const EDIT_REPORT_02_XLD_API  = {
  url:serviceBack +  "/report02xld/",
};

export const DEL_REPORT_02_XLD_API  = {
  url:serviceBack +  "/report02xld/",
};
export const GET_REPORT_02_XLD_BY_ID_API = {
  url:serviceBack + "/report02xld/",
  method: "GET",
};

export const GET_ALL_GOVERMENT_API = {
  url:serviceBackend + "/govagencies/",
  method: "GET",
};
export const GET_ALL_REPORTING_PRRIOD_API = {
  url:serviceBack + "/dc_reporting_periods/",
  method: "GET",
};

export const GET_PRRIOD_BY_GOV_AND_REPORT_API = {
  url:serviceBack + "/dc_reporting_periods/get-by-govId-and-reportId-and-year/",
  method: "GET",
};
export const GET_PRRIOD_BY_REPORT_API = {
  url:serviceBack + "/dc_reporting_periods/get-by-reportId-and-year/",
  method: "GET",
};
