const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";
const serviceBack = process.env.VUE_APP_SERVICE_BACKEND || "";

export const GET_Orgarizations_API = {
  url: serviceCate + "/organizations/",
};
export const ADD_Orgarizations_API = {
  url: serviceCate + "/organizations/",
};

export const EDIT_Orgarizations_API = {
  url: serviceCate + "/organizations/",
};

export const DEL_Orgarizations_API = {
  url: serviceCate + "/organizations/",
};

export const EDIT_UpdateStatusOrgarizations_API = {
  url: serviceCate + "/organizations/update-status/",
};

export const EDIT_ConfirmDuplicateOrgarizations_API = {
  url: serviceCate + "/organizations/confirm-duplicate/",
};

export const GET_CheckOrgarizationInUsed_API = {
  url: serviceBack + "/inspectionobject/checkOrganizations/",
};
