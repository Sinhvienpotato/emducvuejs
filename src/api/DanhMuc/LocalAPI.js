const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";
export const GET_Local_API = {
  url: serviceCate + "/local/",
};
export const ADD_Local_API = {
  url: serviceCate + "/local/",
};
export const EDIT_Local_API = {
  url: serviceCate + "/local/",
};
export const DEL_Local_API = {
  url: serviceCate + "/local/",
};

export const EDIT_UPDATE_STATUS_LOCAL_API = {
  url: serviceCate + "/local/update-status/",
};

export const GET_COMMUNE_BY_DISTRICT_ID_API = {
  url: serviceCate + "/local/get-commune-by-districtId/",
};

export const GET_LIST_PROVINCE_API = {
  url: serviceCate + "/local/get-list-province/",
};

export const GET_LOCAL_BY_PROVINCE_ID_API = {
  url: serviceCate + "/local/get-local-by-provinceId/",
};
