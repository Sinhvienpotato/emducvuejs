const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";

export const GET_COMMUNE_API = {
  url: serviceCate + "/cg_commune/",
};

export const ADD_COMMUNE_API = {
  url: serviceCate + "/cg_commune/",
};
export const EDIT_COMMUNE_API = {
  url: serviceCate + "/cg_commune/",
};
export const DEL_COMMUNE_API = {
  url: serviceCate + "/cg_commune/",
};

export const GET_COMMUNE_BY_DISTRICT_ID_API = {
  url: serviceCate + "/cg_commune/get-by-districtId/",
};

export const GET_COMMUNE_BY_PROVINCE_ID_API = {
  url: serviceCate + "/cg_commune/get-by-provinceId/",
};
