const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";

export const GET_DISTRICT_API = {
  url: serviceCate + "/cg_district/",
};

export const ADD_DISTRICT_API = {
  url: serviceCate + "/cg_district/",
};
export const EDIT_DISTRICT_API = {
  url: serviceCate + "/cg_district/",
};
export const DEL_DISTRICT_API = {
  url: serviceCate + "/cg_district/",
};

export const GET_DISTRICT_BY_PROVINCE_ID_API = {
  url: serviceCate + "/cg_district/get-by-provinceId/",
};
