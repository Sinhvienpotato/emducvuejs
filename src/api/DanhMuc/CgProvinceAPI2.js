const serviceCate = process.env.VUE_APP_SERVICE_CATEGORY || "";
export const GET_PROVINCE_API = {
  url: serviceCate + "/cg_province/",
};
export const ADD_PROVINCE_API = {
  url: serviceCate + "/cg_province/",
};
export const EDIT_PROVINCE_API = {
  url: serviceCate + "/cg_province/",
};
export const DEL_PROVINCE_API = {
  url: serviceCate + "/cg_province/",
};
