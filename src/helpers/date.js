export function dateStringToTimestamp(dateString, parst) {
  // Split the date string into day, month, and year components
  const parts = dateString.split(parst);

  // Ensure that there are three parts
  if (parts.length !== 3) {
    throw new Error("Invalid date format. Use dd/mm/yyyy.");
  }

  // Parse day, month, and year as integers
  const day = parseInt(parts[0], 10);
  const month = parseInt(parts[1], 10) - 1; // Subtract 1 because months are zero-based
  const year = parseInt(parts[2], 10);

  // Create a Date object with the parsed components
  const date = new Date(year, month, day);

  // Get the timestamp in milliseconds
  const timestamp = date.getTime();

  return timestamp;
}
