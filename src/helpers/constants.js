export const agencies = [];

export const reports = {
  QLNN01: "QLNN01",
  TTL: "TTL",
  TCD01: "TCD01",
  XLD02: "XLD02",
  XLD03: "XLD03",
  KQGQ01: "KQGQ01",
  QLNN03: "QLNN03",
  PCTN01: "PCTN01",
  PCTN02: "PCTN02",
  PHTNTT: "PHTNTT",
};
