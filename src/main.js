import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import FontAwesome from '@/plugins/FontAwesome'
import ClickOutsideDirective from '@/plugins/ClickOutsideDirective'
import PageScrollDirective from '@/plugins/PageScrollDirective'
import VeeValidatePlugin from '@/plugins/VeeValidatePlugin'
// Import one of the available themes
import 'element-plus/dist/index.css'
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
// handle router permission
import './utils/permission'
import 'material-icons/iconfont/material-icons.css'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import i18n from '@/lang/i18n';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faUser, faFileExcel, faUsers, faLock, faFile, faCrown } from '@fortawesome/free-solid-svg-icons';
import ElementPlus from 'element-plus'
// import các components dùng chung ở đây
import ValidationForm from './components/ValidationForm.vue'
import PageTT from './components/PageTT.vue';
import ShowToast from "./components/ShowToast.vue";
import ButtonApp from './components/ButtonApp.vue';


const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674',
};
export const forumApp = createApp(App) 
library.add(faHome, faUser, faFileExcel, faUsers, faLock, faFile, faCrown);
forumApp.use(router)
forumApp.use(store)
forumApp.use(FontAwesome)
forumApp.use(ClickOutsideDirective)
forumApp.use(PageScrollDirective)
forumApp.use(VeeValidatePlugin)
forumApp.use(VueSweetalert2, options)
forumApp.use(i18n)
forumApp.use(ElementPlus)
// import các components dùng chung ở đây
forumApp.component('ValidationForm' ,ValidationForm )
forumApp.component('PageTT' ,PageTT )
forumApp.component('ShowToast' ,ShowToast )
forumApp.component('ButtonApp' ,ButtonApp )
const requireComponent = require.context('./components', true, /App[A-Z]\w+\.(vue|js)$/)
requireComponent.keys().forEach(function (fileName) {
  let baseComponentConfig = requireComponent(fileName)
  baseComponentConfig = baseComponentConfig.default || baseComponentConfig
  const baseComponentName = baseComponentConfig.name || (
    fileName
      .replace(/^.+\//, '')
      .replace(/\.\w+$/, '')
  )
  forumApp.component(baseComponentName, baseComponentConfig)
})

forumApp.mount('#app')
