import XLSX from "../lib/xlsx.js";
import { isProxy, toRaw } from "vue";

export function importFile(event) {
  alert("Load file excell ... ");

  this.selectedFile = event.target.files[0];

  if (this.selectedFile) {
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = new Uint8Array(e.target.result);
      const workbook = XLSX.read(data, { type: "array" });
      const worksheet = workbook.Sheets[workbook.SheetNames[0]];
      const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
      let rawData = jsonData;
      if (isProxy(jsonData)) {
        rawData = toRaw(jsonData);
      }

      this.item = jsonData;

      for (var i = 0; i <= jsonData.length; i++) {
        this.item[i].id = rawData[i][0];
        this.item[i].name = rawData[i][1];
        this.item[i].msv = rawData[i][2];
        this.item[i].lop = rawData[i][3];
        this.item[i].detai = rawData[i][4];
        this.item[i].tencongty = rawData[i][5];
      }
    };
    reader.readAsArrayBuffer(this.selectedFile);
  } else {
  }
  return jsonData;
}
