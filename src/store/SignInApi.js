import { post,get} from '../utils/request'
import {LOGIN_API , FORGOT_PASS} from '../api/signInApi'
const actions = {
  signIn(context,body ){
   return post(LOGIN_API.url, body).then(res => {
    return res;
   }).catch (e => {
   })
  },
  forgotPass(context, param) {
    return get(FORGOT_PASS.url + param)
      .then(result => {
        return result;
      })
      .catch(error => {


      });
  },
};
export default {
  namespaced: true,
    actions
  }