import { createStore } from "vuex";
import User from "../store/User";
import SignInApi from "../store/SignInApi";
export default createStore({
  modules: {
    User,
   
    SignInApi,
 
  },
  plugins: [],
  state() {
    return {
      showLoading: false,
    };
  },
});
