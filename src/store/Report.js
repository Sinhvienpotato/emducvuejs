import { get, post, put, del} from '../utils/request'
import { 
    GET_REPORT_API, 
    ADD_REPORT_API, 
    EDIT_REPORT_API
 } from '../api/DanhMuc/ReportAPI'
const actions = {
  getReports(context, param) {
    return get(GET_REPORT_API.url + param)
      .then(result => {
        return result;
      })
      .catch(error => {


      });
  },

  addReport(context, body) {
    return post(ADD_REPORT_API.url, body).then(res => {
      return res;
    }).catch(e => {

    })
  },

  editReport(context, body) {
    return put(EDIT_REPORT_API.url + body.id, body.form).then(res => {
      return res;
    }).catch(e => {

    })
  },

};
export default {
  namespaced: true,
  actions
}