import { get, post, put, del } from "../utils/request";
// import { get, post, put, del } from "../utils/request";

import {
  ADD_Local_API,
  GET_Local_API,
  EDIT_Local_API,
  DEL_Local_API,
  EDIT_UPDATE_STATUS_LOCAL_API,
  GET_COMMUNE_BY_DISTRICT_ID_API,
  GET_LIST_PROVINCE_API,
  GET_LOCAL_BY_PROVINCE_ID_API,
} from "../api/DanhMuc/LocalAPI";

const actions = {
  async getLocal(context, param) {
    return await get(GET_Local_API.url + param)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },

  addLocal(context, body) {
    return post(ADD_Local_API.url, body)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },

  editLocal(context, body) {
    return put(EDIT_Local_API.url + body.id, body.form)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },
  deleteLocal(context, ID) {
    return del(DEL_Local_API.url + ID)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        throw e;
      });
  },
  updateStatusLocal(context, body) {
    return put(EDIT_UPDATE_STATUS_LOCAL_API.url + body.id, body.form)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },
  async getCommuneByDistrictId(context, param) {
    return await get(GET_COMMUNE_BY_DISTRICT_ID_API.url + param)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
  async getListProvince(context) {
    return await get(GET_LIST_PROVINCE_API.url)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
  async getLocalById(context, param) {
    return await get(GET_Local_API.url + param)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
  async getLocalByProvinceId(context, param) {
    return await get(
      GET_LOCAL_BY_PROVINCE_ID_API.url + param.provinceId + "/" + param.level
    )
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
};

export default {
  namespaced: true,
  actions,
};
