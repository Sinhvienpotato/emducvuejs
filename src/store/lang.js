import { setLocale } from '@vee-validate/i18n';

const SET_LANG = "SET_LANG";

const state = {
  currentLocale: null
};

const mutations = {
  SET_LANG(state, { vue, lang }) {
    state.currentLocale = lang;
    vue.$i18n.locale = lang;
    setLocale(lang);
  }
};

const actions = {
  setLang({ commit }, { vue, lang }) {
    commit(SET_LANG, { vue, lang });
  }
};

const storeLang = {
  state,
  actions,
  mutations
};

export default storeLang;
