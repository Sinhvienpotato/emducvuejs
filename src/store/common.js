const actions = {};

 const state = {
  couts : 0,
  showToastCommon: false, 
  toastMessage: "",
  statusToast: 0,
  data: null,
};
 const mutations = {
  setData(state, payload) {
    state.data = payload;
  },

  openToast(statusT, messT) {

   // alert(state.showToastCommon)
    state.showToastCommon = !state.showToastCommon;
    state.toastMessage = messT;
    state.statusToast = statusT;
 
  },
};
const getters = {
  getData(state) {
    return state.data;
  },

  
  showToastCommon : state => state.showToastCommon,
  toastMessage: state => state.toastMessage,
  statusToast :  state => state.statusToast,
};

export default {
  namespaced: true,
  actions,
  state,
  mutations,
  getters,
};
