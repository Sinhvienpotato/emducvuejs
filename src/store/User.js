import { get, post, put, del } from "../utils/request";
//import các phương thức cần sử dụng từ request
import {
  GET_USER_API,
  ADD_USER_API,
  EDIT_USER_API,
  DEL_USER_API,
  CHANGE_PASS_USER,
  LOG_OUT,
  GET_USER_BY_GOV_API,
  GET_USER_BY_DEP_API,
  CHANGE_PASS,
  USER_INFO
} from "../api/PageAPI/UserApi";
// import Url và tên các phương thức tương ứng
const actions = {
  getUser(context, param) {
    return get(GET_USER_API.url + param)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
  getUserByGov(context, id) {
    return get(GET_USER_BY_GOV_API.url + id)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },
  getUserByDep(context, id) {
    return get(GET_USER_BY_DEP_API.url + id)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },

  

  addUser(context, body) {
    return post(ADD_USER_API.url, body)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },

  editUser(context, body) {
    return put(EDIT_USER_API.url + body.id, body.form)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },
    changePassWord(context, body) {
    return put(CHANGE_PASS_USER.url + body.id, body.form)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },

  deleteUser(context, ID) {
    return del(DEL_USER_API.url + ID)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        throw e;
      });
  },

  changePass(context, body) {
    return put( CHANGE_PASS.url, body.form)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },
  getUserInfo(context, param) {
    return get( USER_INFO.url+ param)
      .then((result) => {
        return result;
      })
      .catch((e) => {
        console.log(e);
      });
  },

  logOut(context, body) {
    return post(LOG_OUT.url, body)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        //xử lý ngoại lệ xảy ra ở đây
      });
  },
};
export default {
  namespaced: true,
  actions,
};
