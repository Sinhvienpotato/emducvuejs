import axios from "axios";
const urlBase =   process.env.VUE_APP_URL_BASE || "http://14.177.236.88:881/"
const serviceHis = process.env.VUE_APP_SERVICE_HISTORY || ""
const service = axios.create({
  //ip config url base
  baseURL: urlBase,
  timeout: 60000, // request timeout
});

function ipAddress(status, hanhdong) {
  axios
    .get("https://api.ipify.org?format=json")
    .then((response) => {
      const publicIp = response.data.ip;
      window.localStorage.setItem("ip", publicIp);
      logApiCall(status, hanhdong);
    })
    .catch((error) => {
      console.error(error);
    });
}
function logApiCall(status, hanhdong) {
  const username = window.localStorage.getItem("username");
  const ip = window.localStorage.getItem("ip");
  if (ip == null || ip == "") {
    ipAddress(status, hanhdong);
  }

  const logData = {
    taiKhoan: username,
    diaChiIP: ip,
    diaChiMAC: ip,
    noiDung: "Hành Động: " + hanhdong,
    tenNguoiNhan: ip,
    trangThaiLog: status,
  };

service.post(serviceHis + "/historycore", logData)
    .then((response) => {
      
    })
    .catch((error) => {
  
    });
}

service.interceptors.request.use(
  (config) => {
    const token = window.localStorage.getItem("x-Ricoh-token");
    if (token) {
      config.headers["x-access-token"] = `${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
service.interceptors.response.use(
  function (response) {
  
    return response;
  },
  function (error) {
    throw error;
    return Promise.reject(error);
  }
);
export function get(url) {
  return new Promise((resolve, reject) => {
    service
      .get(url)
      .then((response) => {
        logApiCall(1, url + " phương thức GET");
        resolve(response.data);
      })
      .catch((error) => {
        logApiCall(0, url + " phương thức GET");
      });
  });
}
export function post(url, data) {
  return new Promise((resolve, reject) => {
    service
      .post(url, data)
      .then((response) => {
        logApiCall(
          1,
          "thành công với data " +
          data +
          " response trả về là: " +
          response.data +
          url +
          " phương thức POST"
        );
        resolve(response.data);
      })
      .catch((error) => {
        logApiCall(0, "thất bại vì lỗi : " + error + url + " phương thức POST");
        reject(error);
      });
  });
}
export function put(url, data) {
  return new Promise((resolve, reject) => {
    service
      .put(url, data)
      .then((response) => {
        logApiCall(1, "thành công với data " + data + url + " phương thức PUT");
        resolve(response.data);
      })
      .catch((error) => {
        logApiCall(0, "thất bại vì lỗi : " + error + url + " phương thức PUT");
        reject(error);
      });
  });
}

export function del(url) {
  return new Promise((resolve, reject) => {
    service
      .delete(url)
      .then((response) => {
        logApiCall(1, "thành công" + url + " phương thức DELETE");
        resolve(response.data);
      })
      .catch((error) => {
        logApiCall(
          0,
          "thất bại vì lỗi : " + error + url + " phương thức DELETE"
        );
        reject(error);
      });
  });
}
